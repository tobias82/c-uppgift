#include <iostream>
#include <vector>
#include <string>
#include <array>

using namespace std;


float func_Hypotenusa(float bas, float hojd){
    float hypotenusa = 0;
    //Gör en funktion som räknar ut hypotenusan när
    //man får reda på basen och höjden av en rätvinklig triangel
    return hypotenusa;
}

float func_rArea(float bas, float hojd){
    float area = 0;
    //Gör en funktion som räknar ut area av en rektangel med värdena som fås av användaren.
    //Returnera arean
    return area;
}

float func_Medelvarde(int lista_med_varden[], antal){
    float medel = 0;
    //Du får en lista av användaren med massa heltals-siffror.
    //Gör en funktion som räknar ut medelvärdet av siffrorna i denna lista. Returnera resultatet.
    return medel;
}

bool func_isPalindrom(string mening){
    bool palindrome = false;
    int length = mening.size();
    float middle = length/2;
    cout << middle;
    //Ett palindrom är en sträng/mening/ord som läses likadant baklänges som framlänges se exemplen nedan.
    //Ex: "naturrutan", "kajak", "tillit", "mus rev inuits öra, sa röst i universum"
    //Man ignorerar whitespaces och komman (",") etc, bara bokstäverna betyder något.
    return palindrome;
}

void func_Delay(int sekunder){
    //Skapa en funktion som ger en fördröjning på så många sekunder som användaren anger.
    //Vid varje sekunds fördröjning ska funktionen skriva ut en punkt (ex: .... <- efter 4 sec)
}

string func_Sortera(string ord){
    string sorterad_string = "";
    //Skapa en funktion som sorterar en sträng som kommer in i bokstavsordning.
    //Strängen som kommer in kommer innehålla flera ord, och det är de som ska sorteras i bokstavsordning.
    //Orden i strängen som kommer in kommer vara separerade med ett bindestreck, se exempel nedan.
    //Ex: (trollfabrik-tomater-fisk-påfågel) ska komma tillbaka som (fisk-påfågel-tomater-trollfabrik)
    return sorterad_string;
}

bool func_isPrime(int nummer){
    bool test = false;
	//Gör en funtion som kollar om en siffra är ett primtal. Om det är ett primtal skicka tillbaka "True", annars "False".
    return test;
}

int * func_hittaPrimtal(int nummer){
    static int primes[100];
    //Gör en funktion som hittar alla primtal upp till numret som skickats in till funktionen.
    //Spara primtalen i en array.
    //Returnera detta som en array.
    //Använd gärna funktionen som vi skapat för att kolla om ett tal är primtal (func_isPrime())
    return primes;
}

bool func_perfectNum(int nummer){
    bool is_perfect = false;
    //Gör en funktion som provar om ett tal är "perfekt". Vad är ett "perfekt" tal då? Kolla nedan.
    //Ett perfekt tal är ett tal där alla dess delbara heltal summerat är lika med sig själv.
    //Ex: Det första perfekta talet är 6 eftersom 6 är delbart med 1, 2 och 3. Summan av 1 + 2 + 3 = 6.
    //Ledtråd: Använd modulo (%).
    return is_perfect;
}

int func_Fakultet(int nummer){
    int svar = 0;
    //Skapa en funktion som räknar ut "fakulteten" av talet som skickas in.
    //Ett tal i fakultet skrivs "3!" och betyder att man multiplicerar talet med alla heltal som är lägre än sig själv.
    //I ovanstående exempel skulle det bli 3 * 2 * 1 = 6
    //Skickar användaren in en 6:a ska funtionen alltså ta 6*5*4*3*2*1 = 720
    //Returnera svaret
    return svar;
}

string func_ceasarCrypt(string mening, int nummer){
    string krypt = "";
    //Skapa en funktion som krypterar en sträng med hjälp av ceasarkryptering.
    //Ceasar-kryptering fungerar på så sätt att man tar en bokstav i taget och flyttar fram den ett visst antal bokstäver framåt.
    //Om jag vill göra en kryptering på en sträng behöver jag först välja antalet tecken jag vill flytta i strängen. I mitt exempel väljer jag 10 steg.
    //"Tobias äger" börjar med att jag kollar 10 bokstäver fram på stora T i en ASCII-tabell vilket blir tecknet ^. Sen fortsätter jag bokstav för bokstav.
    //Färdig kryptering av strängen "Tobias äger": ^ylsk}*Äqo|
    //Ovanstående är gjort för hand men är förhoppningsvis rätt. Varje bokstav 10 steg framåt i ASCII-tabellen.
    //Returnera krypterad text
    return krypt;
}

string func_ceasarDecrypt(string mening, int nummer){
    string decrypt = "";
    //Skapa en motsatt funktion som ovan, men som istället för att gå "nummer" steg framåt i ASCII-tabellen, gå istället bakåt.
    return decrypt;
}

int main(){
    cout << "Välkommen till multiprogrammet som gör det mesta. Välj vilket delprogram du vill köra" << endl;
    cout << "1. Räkna ut hypotenusa" << endl;
    cout << "2. Räkna ut en rektangels area" << endl;
    cout << "3. Räkna ut medelvärdet av en lista" << endl;
    cout << "4. Är en sträng/mening ett palindrom?" << endl;
    cout << "5. Skapa delay på x antal sekunder" << endl;
    cout << "6. Sortera några ord i bokstavsordning" << endl;
    cout << "7. Kolla om ett tal är ett primtal" << endl;
    cout << "8. Kolla vilka primtal som finns upp till ett visst tal" << endl;
    cout << "9. Testa om ett nummer är 'PERFEKT'" << endl;
    cout << "10. Vad blir ett tal i fakultet?" << endl;
    cout << "11. Kryptera en text med ceasarkryptering" << endl;
    cout << "12. Dekryptera en text som använder ceasarkryptering" << endl;
    int val;
    cout << "Skriv in ditt val: ";
    cin >> val;

    switch (val){
        case 1:{
            float bas;
            float hojd;
            float hypo;
            cout << "Du vill räkna ut hypotenusan på en triangel. Skriv in bas och höjd för att få reda på hypotenusan." << endl;
            cout << "Skriv in bas: ";
            cin >> bas;
            cout << "Skriv in hojd: ";
            cin >> hojd;
            hypo = func_Hypotenusa(bas, hojd);
            cout << "Hypotenusan är: " << hypo << endl;
            break;
        }
        case 2:{
            float sida1;
            float sida2;
            float area;
            cout << "Vill du veta arean av en rektangel? Fyll i hur långa sidorna i nedan och låt funktionen göra jobbet" << endl;
            cout << "Skriv in första sidans längd: ";
            cin >> sida1;
            cout << "Skriv in andra sidans längd: ";
            cin >> sida2;
            area = func_rArea(sida1, sida2);
            cout << "Rektangelns area är: " << area;
            break;
        }
        case 3:{
            int lista[100];
            int antalnr;
            float svar;
            cout << "Du söker medelvärdet av en massa värden. Följ instruktionerna i programmet och låt funktionen göra sin magic." << endl;
            cout << "Skriv in hur många nummer du vill använda att få ut medelvärde för: ";
            cin >> antalnr;
            for (int i; i<antalnr; i++){
                cout << "Skriv in värde "<< i << ": ";
                cin >> lista[i];
            }
            svar = func_Medelvarde(lista, antalnr);
            cout << "Medelvärdet är: " << svar << endl;
            break;
        }
        case 4:{
            string pal_test;
            cout << "Är en mening eller ett ord ett palindrom? Testa och skriv ett ord och se resultatet!" << endl;
            cout << "Skriv ordet/meningen du vill testa om det är ett palindrom: ";
            cin >> pal_test;
            if (func_isPalindrom(pal_test)){
                cout << pal_test << " IS A FRICKIN' PALINDROME, MAN!" << endl;
            }
            else{
                cout << "No luck.. " << pal_test << "is no palindrome" << endl;
            }
            break;
        }
        case 5:{
            string ord;
            int sek;
            cout <<"Så det finns en funktion som skapar delay? Ja, om nån har programmerat den! prova och skriv nåt å se om det blir delay." << endl;
            cout << "Skriv in det ord du vill ska skrivas ut med delay: ";
            cin >> ord;
            cout << "Skriv in hur många sekunder du vill att delayen ska vara: ";
            cin >> sek;
            func_Delay(sek);
            cout << ord << endl;
            break;
        }
        case 6:{
            string ord;
            string sorterat_ord;
            cout <<"Sortera en sträng i bokstavsordning. Skriv '-' (ett bindestreck) mellan orden du vill sortera" << endl;
            cout << "Exempel: hej-yggdrasil-tomte-bonde" << endl;
            cout << "Skriv in det du vill sortera i bokstavsordning: ";
            cin >> ord;
            sorterat_ord = func_Sortera(ord);
            cout <<"Först skrev du in: " << ord << endl;
            cout <<"Det blev till: ";
            cout <<sorterat_ord;
            break;
        }
        case 7:{
            int tal;
            cout << "Du vill prova om ett tal är primtal? Prova på!" << endl;
            cout << "Skriv in ett tal och se om det är ett primtal: ";
            cin >> tal;
            cout <<tal << " är ett primtal?" << endl;
            cout <<func_isPrime(tal) << endl;
            break;
        }
        case 8:{
            int varde;
            cout << "Dags att prova att få en lista med massa jäkla primtal upp till ett visst värde." << endl;
            cout << "Skriv in hur högt värde du vill hitta alla primtal upp till: ";
            cin >> varde;
            int *all_primes;
            all_primes = func_hittaPrimtal(varde);
            cout << "Denna lista som skrivs ut nedan är alla primtal upp till det tal du skrev in: ";
            for (int i; i<sizeof(all_primes); i++){
                cout << all_primes[i] << ", ";
            }
            break;
        }
        case 9:{
            int tal;
            cout <<"Testa om ett nummer är perfekt!" << endl;
            cout << "Skriv in det tal du vill testa: ";
            cin >> tal;
            cout << func_perfectNum(tal) << endl;
            break;
        }
        case 10:{
            int tal;
            int svar;
            cout << "Skriv in det tal du vill prova i fakultet: ";
            cin >> tal;
            svar = func_Fakultet(tal);
            cout << tal << " i fakultet blir: " << svar << endl;
            break;
        }
        case 11:{
            int chiffer;
            string text;
            string kryptad;
            cout << "Kryptera en text! Skriv in chiffer (ett heltal mellan 1-20): ";
            cin >> chiffer;
            cout << "Skriv in texten du vill kryptera: ";
            cin >> text;
            kryptad = func_ceasarCrypt(text, chiffer);
            cout <<"Din text krypterad: ";
            cout <<kryptad;
            break;
        }
        case 12:{
            int chiffer;
            string text;
            string decrypt;
            cout << "Decrypt dax. Du behöver dels den krypterade strängen och dess chiffer" << endl;
            cout << "Skriv in det chiffer som användes vid kryptering (ett heltal mellan 1-20): ";
            cin >> chiffer;
            cout <<"Skriv in texten du vill dekryptera: ";
            cin >> text;
            decrypt = func_ceasarDecrypt(text, chiffer);
            break;
        }
        default:{
            cout <<"Whatever" << endl;
            break;
        }
    }
}
