# Programmering - C++
Detta projekt innehåller lite uppgifter som är anpassat för C++.

# Uppgift - Sammanfattning
Denna kodfil är uppdelad i massa funktioner. Funktionerna är tänkt att göras när ni känner er redo att ge er på att lösa funktionen.

# Uppgiftsupplägg
1. Klona repositoriet till er dator (för att göra detta behöver ni ha installerat git).
2. Skapa din branch. (den ska ha ditt namn)
3. Öppna c++-filen med valfri redigerare.
4. Programmera funktionen du vill prova på.
5. Testa så funktionen fungerar tillfredställande.
6. Commita och pusha förändringarna till din branch.
